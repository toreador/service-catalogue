/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.eng.toreador.security;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by ordeal on 11/07/17.
 */

public interface RoleRepository extends JpaRepository<Role, Long>
{
    public List<Role> findByName(String role);


    /*private final JPAQueryFactory jpaQueryFactory;
    private EntityManager em;

    public RoleRepository(EntityManager entityManager)
    {
        super(entityManager, Role.class);
        jpaQueryFactory = new JPAQueryFactory(entityManager);
    }

    public List<Role> findByName(String role)
    {
        QRole qrole = QRole.role;
        return jpaQueryFactory.selectFrom(qrole).where(qrole.name.eq(role)).fetch();
    }

    @Override
    @Transactional
    public Role save(Role entity)
    {
        return super.save(entity);
    }*/
}
