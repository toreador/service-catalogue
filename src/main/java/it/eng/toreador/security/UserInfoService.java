/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.eng.toreador.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ordeal on 11/07/17.
 */
@Transactional
@Component("userDetailsService")
public class UserInfoService implements UserDetailsService
{
    @Inject
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException
    {
        User user = userRepository.findByEmail(email);
        if (user == null)
        {
            return new org.springframework.security.core.userdetails.User(
                    " ", " ", true, true, true, true, new ArrayList<>());
        }

        return new org.springframework.security.core.userdetails.User(
                user.getEmail(), user.getPassword(), user.isEnabled(), true, true,
                true, getAuthorities(user));
    }

    private Collection<? extends GrantedAuthority> getAuthorities(
            User user)
    {

        return getGrantedAuthorities(user.getRoles().stream().map(r -> r.getName()).collect(Collectors.toList()));
    }


    private List<GrantedAuthority> getGrantedAuthorities(List<String> grantedAuthorities)
    {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : grantedAuthorities)
        {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }

}
