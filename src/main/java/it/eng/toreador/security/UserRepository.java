/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.eng.toreador.security;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ordeal on 11/07/17.
 */
public interface UserRepository extends JpaRepository<User, Long>
{
    public User findByEmail(String email);
   /* private final JPAQueryFactory jpaQueryFactory;
    private EntityManager em;

    public UserRepository(EntityManager entityManager)
    {
        super(entityManager, User.class);
        this.em = entityManager;
        jpaQueryFactory = new JPAQueryFactory(entityManager);
    }

    public User findByEmail(String email)
    {
        QUser quser = QUser.user;
        return jpaQueryFactory.selectFrom(quser).where(quser.email.eq(email)).fetchOne();
    }

    @Override
    @Transactional
    public User save(User entity)
    {
        return super.save(entity);
    }*/
}
