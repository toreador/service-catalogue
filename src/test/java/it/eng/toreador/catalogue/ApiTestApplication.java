/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.eng.toreador.catalogue;

import it.eng.toreador.security.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by ordeal on 26/01/18.
 */
@SpringBootApplication
@Configuration
@EnableWebSecurity
@EntityScan(basePackages = {"it.eng.toreador.security", "it.eng.toreador.catalogue"})
@EnableJpaRepositories(basePackages = {"it.eng.toreador.security", "it.eng.toreador.catalogue"})
public class ApiTestApplication extends WebSecurityConfigurerAdapter
{
    @Bean
    public RestAuthenticationEntryPoint restAuthenticationEntryPoint()
    {
        return new RestAuthenticationEntryPoint();
    }

    @Bean
    public UserDetailsService userDetailsService()
    {
        return new UserInfoService();
    }
    @Bean
    public UserService userService()
    {
        return new UserService();
    }

    @Bean
    RoleService roleService()
    {
        return new RoleService();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http
                .csrf().disable()
                .exceptionHandling()
                .and()
                .headers().frameOptions().disable()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/api/**").hasAnyRole(AuthoritiesConstants.USER, AuthoritiesConstants.ADMIN).
                antMatchers(HttpMethod.POST, "/api/**").hasAnyRole(AuthoritiesConstants.USER, AuthoritiesConstants.ADMIN).
                antMatchers(HttpMethod.PUT, "/api/**").hasAnyRole(AuthoritiesConstants.USER, AuthoritiesConstants.ADMIN).
                antMatchers(HttpMethod.DELETE, "/api/**").hasRole(AuthoritiesConstants.ADMIN).and()
                .httpBasic();
    }
}
