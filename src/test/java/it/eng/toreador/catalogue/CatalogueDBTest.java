/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.eng.toreador.catalogue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.*;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.*;

/**
 * Created by ordeal on 31/05/17.
 */
@RunWith(SpringRunner.class)
@DirtiesContext
@SpringBootTest(classes = {ApiTestApplication.class, CatalogueTestBundle.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CatalogueDBTest
{
    private final Logger log = LoggerFactory.getLogger(CatalogueDBTest.class);
    @Inject
    private CatalogueService catalogueService;
    @Inject
    private ServiceService serviceService;

    @Inject
    private FrameworkService frameworkService;

    private Catalogue catalogue;

    @Before
    public void setup()
    {

        Collection<Service> services = new ArrayList<Service>();
        catalogue = new Catalogue(services);
        List<Catalogue> catalogues = new ArrayList<>();
        catalogues.add(catalogue);
        catalogue.setName("test2");
        catalogue.setServices(services);
        Framework testFW1 = new Framework().setName("TestFW1").setVersion("fw1");
        Framework testFW2 = new Framework().setName("TestFW2").setVersion("fw2");
        testFW1 = frameworkService.createFramework(testFW1);
        testFW2 = frameworkService.createFramework(testFW2);
        Service service1 = new Service();
        service1.setDescription("Description test1");
        service1.setArea(Service.AREA.INGESTION);
        service1.setUrl("file:///path1");
        service1.setFramework(testFW1);
        Set<Property> properties = new HashSet<>();
        List<CategoryMapping> mappings = new ArrayList<>();
        mappings.add(new CategoryMapping().setName("mapping1").setIntValue(34));
        properties.add(new StaticProperty().setKey("p1").setType(Property.PARAMTYPE.STRING).setDescription("Desc p1").setMandatory(true).setService(service1));
        properties.add(new StaticProperty().setKey("p2").setType(Property.PARAMTYPE.STRING).setDescription("Desc p2").setMandatory(true).setService(service1));
        properties.add(new TuningProperty().setKey("p3").setType(Property.PARAMTYPE.STRING).setDescription("Desc p3").setService(service1));
        properties.add(new TuningProperty().setKey("p4").setType(Property.PARAMTYPE.STRING).setDescription("Desc p4").setMandatory(true).setService(service1));
        properties.add(new ApplicationProperty().setKey("p5").setType(Property.PARAMTYPE.STRING).setDescription("Desc p5").setMandatory(true).setService(service1));
        properties.add(new TuningProperty().setMappings(mappings).setKey("p6").setType(Property.PARAMTYPE.STRING).setDescription("Desc p6").setMandatory(true).setService(service1));
        service1.setName("service1type").setProperties(properties);

        service1.setCatalogues(catalogues);
        services.add(service1);

        Metric m1 = new Metric().setName("m1").setMeasure(Metric.MEASURE.BYTES).setUrl("${historyserverurl}/applications/").setMinValue(40.000).setMaxValue(510.00);
        Metric m2 = new Metric().setName("m2").setMeasure(Metric.MEASURE.MILLISECONDS).setUrl("${historyserverurl}/applications/").setMinValue(45.000).setMaxValue(515.00);
        service1.getMetrics().add(m1);
        service1.getMetrics().add(m2);


        Service service2 = new Service();
        service2.setDescription("Description test2");
        service2.setArea(Service.AREA.INGESTION);
        service2.setUrl("file:///path2");
        service2.setFramework(testFW2);

        Set<Property> properties2 = new HashSet<>();
        properties2.add(new StaticProperty().setKey("p11").setType(Property.PARAMTYPE.STRING).setDescription("Desc p11").setMandatory(true).setService(service2));
        properties2.add(new StaticProperty().setKey("p22").setType(Property.PARAMTYPE.STRING).setDescription("Desc p22").setMandatory(true).setService(service2));

        properties2.add(new TuningProperty().setKey("p23").setType(Property.PARAMTYPE.STRING).setDescription("Desc p23").setService(service2));
        properties2.add(new TuningProperty().setKey("p24").setType(Property.PARAMTYPE.STRING).setDescription("Desc p24").setMandatory(true).setService(service2));
        properties2.add(new ApplicationProperty().setKey("p25").setType(Property.PARAMTYPE.STRING).setDescription("Desc p25").setMandatory(true).setService(service2));
        service2.setName("service1type2").setProperties(properties2);

        service2.setCatalogues(catalogues);
        services.add(service2);

        catalogue = catalogueService.saveCatalogue(catalogue);

    }


    @Test
    public void test1() throws InterruptedException
    {

        Collection<String> types = serviceService.getServiceNames(Service.AREA.INGESTION.toString());
        assertThat(types, contains("service1type", "service1type2"));
    }


    @Test
    public void test2() throws InterruptedException
    {
        Framework testFW1 = new Framework().setName("TestFW3").setVersion("fw3");
        testFW1 = frameworkService.createFramework(testFW1);
        List<Catalogue> catalogues = new ArrayList<>();
        catalogues.add(catalogue);
        Service service1 = new Service();
        service1.setDescription("Description test1");
        service1.setArea(Service.AREA.INGESTION);
        service1.setUrl("file:///path3");
        service1.setFramework(testFW1);
        Set<Property> parameters3 = new HashSet<>();
        parameters3.add(new ApplicationProperty().setKey("p1").setType(Property.PARAMTYPE.STRING).setDescription("Desc p1").setMandatory(true).setService(service1));
        service1.setName("service3type").setProperties(parameters3);
        service1.setCatalogues(catalogues);
        catalogue.getServices().add(service1);
        Catalogue ctg = catalogueService.saveCatalogue(catalogue);

        assertTrue(ctg.getServices().size() == 3);
        service1 = ctg.getServices().stream().filter(s -> s.getName().equals("service3type")).findFirst().get();
        assertTrue(service1.getProperties().size() == 1);
        Long serviceId = service1.getId();
        ctg.getServices().remove(service1);
        catalogueService.saveCatalogue(ctg);
        serviceService.delete(serviceId);
        ctg = catalogueService.getCatalogue("test2");
        assertTrue(ctg.getServices().size() == 2);


    }


    @Test
    public void test3() throws InterruptedException
    {
        Catalogue ctg = catalogue;
        Collection<Service> sons = serviceService.getCatalogueServices("test2");
        assertTrue(sons.size() == 2);
        Service s = ctg.getServices().stream().filter(ser -> ser.getName().equals("service1type")).findFirst().get();
        ctg.getServices().remove(s);
        catalogueService.saveCatalogue(ctg);
        sons = serviceService.getCatalogueServices("test2");
        assertTrue(sons.size() == 1);
        ctg.getServices().add(s);
        catalogueService.saveCatalogue(catalogue);

    }

    @Test
    public void test4() throws InterruptedException
    {

        Framework testFW1 = new Framework().setName("TestFW3").setVersion("fw3");
        testFW1 = frameworkService.createFramework(testFW1);
        Service service1 = new Service();
        service1.setDescription("Description test2");
        service1.setArea(Service.AREA.INGESTION);
        service1.setUrl("file:///path4");
        service1.setFramework(testFW1);
        Set<Property> parameters3 = new HashSet<>();
        parameters3.add(new TuningProperty().setKey("p3").setType(Property.PARAMTYPE.STRING).setDescription("Desc p3").setMandatory(true).setService(service1));
        parameters3.add(new TuningProperty().setKey("p4").setType(Property.PARAMTYPE.INT).setDescription("Desc p4").setMandatory(true).setService(service1));
        service1.setName("service4type").setProperties(parameters3);
        Catalogue ctg = catalogue;

        service1.getCatalogues().add(ctg);
        ctg.getServices().add(service1);
        ctg = catalogueService.saveCatalogue(ctg);
        assertTrue(ctg.getServices().size() == 3);

        Service sfound = ctg.getServices().stream().filter(s -> s.getName().equals("service4type")).findFirst().get();
        Framework fwFound = sfound.getFramework();
        assertNotNull(fwFound);
        assertNotNull(sfound.getProperties());
        assertEquals(2, sfound.getProperties().size());
        assertTrue(sfound.getProperties().stream().filter(p -> p.getKey().equals("p3")).count() == 1);

        assertNotNull(sfound.getFramework());
        assertEquals("TestFW3", sfound.getFramework().getName());

        ctg.getServices().remove(sfound);
        catalogueService.saveCatalogue(ctg);

        serviceService.delete(sfound.getId());
        ctg = catalogueService.getCatalogue("test2");
        assertTrue(ctg.getServices().size() == 2);

    }

    /*@Test(expected = PersistenceException.class)
    public void test5()
    {
        Catalogue cat = new Catalogue("test");
        catalogueService.saveCatalogue(cat);

    }*/
    @Transactional
    @Test
    public void test6() throws InterruptedException
    {

        Framework testFW1 = new Framework().setName("TestFW6").setVersion("fw6");
        Framework testFW2 = new Framework().setName("TestFW7").setVersion("fw7");
        Service service1 = new Service();
        service1.setDescription("Description test2");
        service1.setArea(Service.AREA.INGESTION);
        service1.setUrl("file:///path6");
        service1.setFramework(testFW1);
        service1 = serviceService.save(service1);
        service1.setArea(Service.AREA.ANALYTICS);
        service1.setFramework(testFW2);
        service1 = serviceService.save(service1);
        assertEquals(Service.AREA.ANALYTICS, service1.getArea());
        assertEquals("fw7", service1.getFramework().getVersion());
        serviceService.delete(service1.getId());
    }

    @Transactional
    @Test
    public void test7() throws InterruptedException
    {
        Service service1;
        Service service2;

        //define service
        service1 = new Service();
        service1.setDescription("Description test1");
        service1.setArea(Service.AREA.INGESTION);
        service1.setUrl("file:///path1");
        Set<Property> properties = new HashSet<>();
        properties.add(new ApplicationProperty().setKey("p1").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p1").setMandatory(true).setService(service1));
        properties.add(new ApplicationProperty().setKey("p2").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p2").setMandatory(true).setService(service1));
        properties.add(new ApplicationProperty().setKey("p3").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p3").setService(service1));
        properties.add(new ApplicationProperty().setKey("p4").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p4").setMandatory(true).setService(service1));
        properties.add(new ApplicationProperty().setKey("p5").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p5").setMandatory(true).setService(service1));
        service1.setName("service1type").setProperties(properties);


        //define service
        service2 = new Service();
        service2.setDescription("Description test1");
        service2.setArea(Service.AREA.ANALYTICS);
        service2.setUrl("file:///path1");
        Set<Property> parameters2 = new HashSet<>();
        parameters2.add(new ApplicationProperty().setKey("p11").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p11").setMandatory(true).setService(service2));
        parameters2.add(new ApplicationProperty().setKey("p23").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p23").setMandatory(true).setService(service2));
        ApplicationProperty property1 = (ApplicationProperty) new ApplicationProperty().setKey("p31").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p31").setService(service2);
        property1.setInputData(true);
        property1.setOutputData(false);
        parameters2.add(property1);
        parameters2.add(new TuningProperty().setKey("p4").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p4").setMandatory(true).setService(service2));
        parameters2.add(new TuningProperty().setKey("p5").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p5").setMandatory(true).setService(service2));
        service2.setName("service2type").setProperties(parameters2);

        service1 = serviceService.save(service1);
        service2 = serviceService.save(service2);
        assertEquals(Service.AREA.ANALYTICS, service2.getArea());
        assertEquals(3, service2.getProperties().stream().filter(p -> p instanceof ApplicationProperty).count());
        assertEquals(2, service2.getProperties().stream().filter(p -> p instanceof TuningProperty).count());
        assertTrue(((ApplicationProperty) service2.getProperties().stream().filter(p -> p.getKey().equals("p31")).findFirst().get()).isInputData());
        assertFalse(((ApplicationProperty) service2.getProperties().stream().filter(p -> p.getKey().equals("p31")).findFirst().get()).isOutputData());
    }


    @Transactional
    @Test
    public void test8() throws InterruptedException
    {
        Service service = serviceService.getAllServices().stream().filter(s -> s.getName().equals("service1type")).findFirst().get();
        assertEquals(2, service.getMetrics().size());
        service.getMetrics().removeIf(m -> m.getName().equals("m1"));
        service = serviceService.save(service);
        assertEquals(1, service.getMetrics().size());

        service.getProperties().stream()
                .filter(p -> p.getKey().equals("p6"))
                .forEach(p -> ((TuningProperty) p).getMappings().stream().findFirst().get().setIntValue(23));
        service = serviceService.save(service);
        TuningProperty prop = (TuningProperty) service.getProperties().stream().filter(p -> p.getKey().equals("p6")).findFirst().get();
        assertEquals(Integer.valueOf(23), prop.getMappings().stream().findFirst().get().getIntValue());
    }

    @After
    public void tearDown() throws InterruptedException
    {

        Collection<Service> services = catalogue.getServices();
        services.forEach(s -> s.setFramework(null));
        services.forEach(s -> serviceService.save(s));
        frameworkService.deleteByName("TestFW1");
        frameworkService.deleteByName("TestFW2");

        services.clear();
        Catalogue cat = catalogueService.saveCatalogue(catalogue);
        serviceService.deleteAll();
        catalogueService.delete(cat.getId());


    }
}