/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.eng.toreador.catalogue;

import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import it.eng.toreador.security.Role;
import it.eng.toreador.security.RoleService;
import it.eng.toreador.security.User;
import it.eng.toreador.security.UserService;
import org.apache.commons.codec.binary.Base64;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsCollectionContaining.hasItems;

/**
 * Created by ordeal on 25/01/18.
 */
@RunWith(SpringRunner.class)
@DirtiesContext
@SpringBootTest(classes = {ApiTestApplication.class, CatalogueTestBundle.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CatalogueIntegrationTest
{

    Service service1;
    Service service2;
    Catalogue catalogue;

    @Autowired
    private ServiceService serviceService;

    @Autowired
    private RoleService roleService;
    @Autowired
    private UserService userService;
    @Inject
    private CatalogueService catalogueService;


    private User user;
    private Role role;
    private Framework fw;

    @Before
    public void setup() throws IOException, InterruptedException
    {

        Collection<Service> services = new ArrayList<Service>();

        catalogue = new Catalogue(services);
        catalogue.setName("test2");
        Property property;

        //define service
        service1 = new Service();
        service1.setDescription("Description test1");
        service1.setArea(Service.AREA.INGESTION);
        service1.setVersion("0.0.1");
        service1.setName("service1");
        service1.setUrl("file:///path1");
        service1.setMode(Service.MODE.BATCH);
        Set<Property> properties = new HashSet<>();
        properties.add(new ApplicationProperty().setInputData(true).setOutputData(false).setKey("p1").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p1").setMandatory(true).setService(service1));
        properties.add(new ApplicationProperty().setKey("p2").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p2").setMandatory(true).setService(service1));
        properties.add(new TuningProperty().setMinValue("34").setMaxValue("354").setMeasure(Metric.MEASURE.MILLISECONDS).setKey("p3").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p3").setService(service1));
        properties.add(property = new TuningProperty().setMinValue("32").setMaxValue("354").setMeasure(Metric.MEASURE.BYTES).setKey("p4").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p4").setMandatory(true).setService(service1));
        properties.add(new StaticProperty().setUri("http://spring-cloud-config:8080").setExternalized(true).setKey("p5").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p5").setMandatory(true).setService(service1));
        service1.setName("service1").setProperties(properties);

        //define categorymapping
        CategoryMapping mapping = new CategoryMapping().setIntValue(1).setName("serializer.type1");
        CategoryMapping mapping2 = new CategoryMapping().setIntValue(2).setName("serializer.type2");
        //set categorymapping
        ((TuningProperty) property).getMappings().add(mapping);
        ((TuningProperty) property).getMappings().add(mapping2);

        //Define metric
        Metric metric = new Metric().setName("m1").setMaxValue(2.0).setMinValue(1.0).setUrl("http://");
        service1.getMetrics().add(metric);


        //define service
        service2 = new Service();
        service2.setName("service2");
        service2.setDescription("Description test1");
        service2.setArea(Service.AREA.ANALYTICS);
        service2.setUrl("file:///path1");
        service2.setVersion("0.0.2");
        service1.setMode(Service.MODE.PROCESSOR);

        fw = new Framework();
        fw.setVersion("fwVersion");
        fw.setName("name");


        Set<Property> parameters2 = new HashSet<>();
        parameters2.add(new ApplicationProperty().setKey("p11").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p11").setMandatory(true).setService(service2));
        parameters2.add(new ApplicationProperty().setKey("p23").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p23").setMandatory(true).setService(service2));
        parameters2.add(new StaticProperty().setKey("p31").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p31").setService(service2));
        parameters2.add(new StaticProperty().setKey("p4").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p4").setMandatory(true).setService(service2));
        parameters2.add(new TuningProperty().setKey("p5").setType(Property.PARAMTYPE.STRING).setDefaultValue("default").setDescription("Desc p5").setMandatory(true).setService(service2));
        service2.setName("service2").setProperties(parameters2);

        role = new Role();
        role.setName("ROLE_ADMIN");
        role = roleService.save(role);

        user = new User();
        user.setLogin("test");
        user.setEmail("test");
        user.setEnabled(true);
        user.setTokenExpired(false);
        user.setPassword("012345678901234567890123456789012345678901234567890123456789");
        userService.save(user);

        user.getRoles().add(role);
        user = userService.save(user);

    }

    private HttpHeaders createHeaders(String username, String password)
    {
        return new HttpHeaders()
        {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("US-ASCII")));
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader);
        }};
    }


    @Test
    public void cataloguesEndpoint_ShouldReturnCatalogueWithService1WithMetricsAndMappings_WhenCatalogueUpdated() throws InterruptedException, IOException
    {


        RequestSpecification request = given().
                auth().
                basic("test", "012345678901234567890123456789012345678901234567890123456789").
                contentType(ContentType.JSON);
        fw = request.
                body(fw).
                post("http://localhost:8091/ServiceCatalogue/api/frameworks").getBody().as(Framework.class);

        service1.setFramework(fw);
        service1 = request.
                body(service1).
                post("http://localhost:8091/ServiceCatalogue/api/services").getBody().as(Service.class);
        service2 = request.
                body(service2).
                post("http://localhost:8091/ServiceCatalogue/api/services").getBody().as(Service.class);
        catalogue = request.
                body(catalogue).
                post("http://localhost:8091/ServiceCatalogue/api/catalogues").getBody().as(Catalogue.class);
        catalogue.getServices().add(service1);
        catalogue.getServices().add(service2);
        catalogue = request.
                body(catalogue).
                put("http://localhost:8091/ServiceCatalogue/api/catalogues").getBody().as(Catalogue.class);
        catalogue.getServices().removeIf(s -> s.getId().equals(service2.getId()));


        request.when().
                body(catalogue).
                put("http://localhost:8091/ServiceCatalogue/api/catalogues").
                then().
                assertThat()
                .body("services.name", hasItems("service1"))
                .body("services.name", not(hasItems("service2")))
                .body("services.find { s -> s.name ==  'service1' }.properties.'it.eng.toreador.catalogue.TuningProperty'.mappings", hasSize(2))
                .body("services.find { s -> s.name ==  'service1' }.metrics", hasSize(1));
    }


    @Test
    public void cataloguesEndpoint_ShouldReturnInternalServerError_WhenCatalogueCreatedWithDetachedServices()
    {
        RequestSpecification request = given().
                auth().
                basic("test", "012345678901234567890123456789012345678901234567890123456789").
                contentType(ContentType.JSON);
        service1 = request.
                body(service1).
                post("http://localhost:8091/ServiceCatalogue/api/services").getBody().as(Service.class);
        service2 = request.
                body(service2).
                post("http://localhost:8091/ServiceCatalogue/api/services").getBody().as(Service.class);
        catalogue.getServices().add(service1);
        catalogue.getServices().add(service2);
        //POST CATALOGUE
        request.when().
                body(catalogue).
                post("http://localhost:8091/ServiceCatalogue/api/catalogues").
                then().
                assertThat().
                statusCode(equalTo(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }


    @Test
    public void cataloguesEndpoint_ShouldReturnCreated_WhenCatalogueCreatedWithTransientServices()
    {
        RequestSpecification request = given().
                auth().
                basic("test", "012345678901234567890123456789012345678901234567890123456789").
                contentType(ContentType.JSON);
        catalogue.getServices().add(service1);
        catalogue.getServices().add(service2);
        //POST CATALOGUE
        request.when().
                body(catalogue).
                post("http://localhost:8091/ServiceCatalogue/api/catalogues").
                then().
                assertThat().
                statusCode(equalTo(HttpStatus.CREATED.value()));
    }


    @After
    public void cleanup() throws InterruptedException
    {

        user.getRoles().clear();
        user = userService.save(user);
        roleService.delete(role);
        userService.delete(user);
        if (catalogue.getId() != null)
            catalogueService.delete(catalogue.getId());

    }

}
