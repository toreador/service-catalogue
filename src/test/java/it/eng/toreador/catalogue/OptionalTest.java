/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.eng.toreador.catalogue;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by ordeal on 31/05/17.
 */
@RunWith(JUnit4.class)
public class OptionalTest
{
    private final Logger log = LoggerFactory.getLogger(OptionalTest.class);


    @Test
    public void test1()
    {
        List<Integer> emptyIntegers = new ArrayList<Integer>();
        List<Integer> nicolaIntegers = new ArrayList<Integer>();
        nicolaIntegers.add(34);
        nicolaIntegers.add(645);
        long test1 = Optional.of(emptyIntegers).filter(t -> !t.isEmpty()).map(result -> new Integer(5))
                .orElse(new Integer(-1));
        long test2 = Optional.of(nicolaIntegers).filter(t -> !t.isEmpty()).map(result -> new Integer(5))
                .orElse(new Integer(-1));
        Assert.assertEquals(-1l, test1);
        Assert.assertEquals(5l, test2);

    }


}